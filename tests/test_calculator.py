import unittest
from core.Calculator import Calculator


class TestCalculatorResults(unittest.TestCase):
    """
    Nominal Test Class for Claculator module. Test classes for All other modules can be written separately too
    """
    def setUp(self):
        self.calculator = Calculator()
        self.test_expressions = {"1+2+3": 6,
                                 "(1/0)  -   2": -2,
                                 "1/0": 0,
                                 "(3-2*(2*(4)/2+3*(7/2)))": -26}

        self.invalid_expressions = ["+2+3+4", "2++3*4", "2/", "*2", "((1+2)", "1+2/((3)))"]

    def test_expression_results(self):
        expressions = self.test_expressions.keys()
        for expr in expressions:
            result = self.calculator.calculate(expr)
            expected_result = self.test_expressions[expr]
            self.assertEqual(result, expected_result, f"{expr} expects {expected_result} gets {result}")

    def test_invalid_expressions(self):
        for expr in self.invalid_expressions:
            result = self.calculator.calculate(expr)
            self.assertFalse(self.calculator.is_valid_expression(expr), f"{expr} is Invalid")
            self.assertEqual(result, 0, f"{expr} is Invalid and expression result is 0")

    def test_calculator_supports_decimals(self):
        expr = "1/2.5"
        result = self.calculator.calculate(expr)
        self.assertTrue(float(result), f"{expr} = {result} is a float")


def main(self):
    unittest.main()


if __name__ == "__main__":
    main()
