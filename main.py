from core.Calculator import Calculator


def get_expression_input(prompt):
    calc = Calculator()
    expr = ""
    while True:
        try:
            expr = str(input(prompt))
        except ValueError:
            print("Invalid Input String")

        if expr == "exit":
            break

        if not calc.is_valid_expression(expr):
            print("The Expression is not a Valid Arithmetic Expression")
            continue
        else:
            result = calc.calculate(expr)
            print(f"expression '{expr}' = {result}")
            continue


if __name__ == '__main__':
    get_expression_input("Enter expression or type 'exit' to stop: ")

