**Basic Bodmas Calculator in Python**

This Implements a basic bodmas Calculator that parses expressions with [+, -, *, /] Operators.

## Summary:

There are multiple different ways to implement a Calculator like this. A few Being:

1. Define A Grammar for the Languge that enforces operator precedence and Then Translate it into OOP classes
2. Use a BinaryTree Node Structure to build an Expression Tree for the expression with all Operands on the Leaf nodes and then traverse the Tree to Evaluate Expression Result
3. Convert infix expression to Postfix expression and then process using a Stack like Structure. This is almost the same  as simulating the Expression Tree method in Step 2.

I chose to Implement the Calculator using the 3rd way. For Iteratability, Extensibility and show-casing some OOP

The Basic steps to the processing are:

1. Prompt User for input
2. Cleaning the expression string: removing whitespaces
3. Validating that the infix expression is resonably well formed.
4. if Valid expression, Tokenize the characters in the string to a list of Objects of the type Token. If Invalid, infor the user and prompt for new input
5. Convert infix tokens List to Post-fix tokens list.
6. Process Post fix list to get expression result
7. Prompt for another valid input, till user types 'exit'


I haven't added a lot of comments in the code itself, But some overview of the classes and methods are shared in the Code for posterity

## Usage:

The Calculator has been implemented in Python. It runs on the terminal. the command to run it is ``python main.py``
The ``core`` package has the core business logic of our Calculator. And ``tests`` package has the nominal unit tests. The tests use the unittest module that is 
packaged with Python so no dependencies have to be installed by the end user.

