from core.TokensUtil import Token, TokenType
import core.Constants as Constants


class Convertor:
    """
    Class Helps convert infix tokens list to postfix tokens list, It uses a list and some helper methods to provide
    stack like functionality to help in the conversion
    """
    # Constructor to initialize the class variables
    def __init__(self):
        self.top = -1
        self.stack = []
        self.output = []
        self.precedence = Constants.OPERATORS_PRECEDENCE

    def __is_empty(self) -> bool:
        return True if self.top == -1 else False

    def __peek(self) -> Token:
        return self.stack[-1]

    def __pop(self) -> Token:
        if not self.__is_empty():
            self.top -= 1
            return self.stack.pop()
        else:
            return Token(token_type=TokenType.OTHER, value="$")

    def __push(self, op: Token):
        self.top += 1
        self.stack.append(op)

    # Check if the precedence of operator is less than the top of stack
    def __not_greater(self, i: Token) -> bool:
        try:
            a = self.precedence[i.tok_value]
            b = self.precedence[self.__peek().tok_value]
            return True if a <= b else False
        except KeyError:
            return False

    # converts given infix expression, to postfix expression
    def get_postfix_tokens(self, exp: [Token]) -> [Token]:

        for i in exp:
            if i.tok_type == TokenType.NUMBER:
                self.output.append(i)

            # If the token is an '(', push to stack
            elif i.tok_type == TokenType.OPEN_PARANTHESIS:
                self.__push(i)

            # If the token is an ')', pop until an '('
            elif i.tok_type == TokenType.CLOSE_PARANTHESIS:
                while ((not self.__is_empty()) and
                       self.__peek().tok_value != '('):
                    a = self.__pop()
                    self.output.append(a)
                if not self.__is_empty() and self.__peek().tok_value != '(':
                    return -1
                else:
                    self.__pop()

            # An operator is encountered
            elif i.tok_type == TokenType.OPERATOR:
                while not self.__is_empty() and self.__not_greater(i):
                    self.output.append(self.__pop())
                self.__push(i)
            # An unrecognized Token has been detected
            elif i.tok_type == TokenType.OTHER:
                raise Exception(f"An unrecognized Token {i.tok_value} has been detected in expression")

        while not self.__is_empty():
            self.output.append(self.__pop())

        return self.output
