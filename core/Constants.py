OPERATORS_lIST = ["+", "-", "*", "/"]
OPERATORS_PRECEDENCE = {'+': 1, '-': 1, '*': 2, '/': 2, '^': 3}

RESERVED_TOKENS = ["+", "-", "*", "/", "(", ")"]
