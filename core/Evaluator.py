from core.TokensUtil import Token, TokenType
from core.OperatorsUtil import OperatorFactory


class Evaluator:
    """
    Class processes the postfix tokens list to get the expression result. Postfix order makes it relatively easy
    to process the string without having to use an Expression Tree. We push Token value on the stack if TokenTyp is
    NUMBER and we Pop last 2 tokens from stack when we encounter an Operator TokenType, get the Operator Object from
    OperatorFactory and call perform_action of the Operator class
    """
    def __init__(self):
        self.stack = []
        self.top = -1
        self.op_factory = OperatorFactory()

    def __pop(self) -> float:
        if self.top == -1:
            return
        else:
            self.top -= 1
            return self.stack.pop()

    def __push(self, i: float):
        self.top += 1
        self.stack.append(i)

    def evaluate(self, expr_tokens: [Token]) -> float:\

        for token in expr_tokens:

            if token.tok_type == TokenType.NUMBER:
                self.__push(float(token.tok_value))

            elif token.tok_type == TokenType.OPERATOR:
                op = self.op_factory.get_operator(token.tok_value)
                operand_right = self.__pop()
                operand_left = self.__pop()

                result = op.perform_operation(operand_left, operand_right)

                self.__push(result)

        return float(self.__pop())
