import core.Constants as Constants
from core.TokensUtil import TokenType


class Utilities:

    @staticmethod
    def is_float(str_num: str) -> bool:
        try:
            float(str_num)
            return True
        except:
            return False

    @staticmethod
    def get_token_type_from_symbol(symbol: str) -> TokenType:
        tok_type = TokenType.OTHER

        if symbol in Constants.OPERATORS_lIST:
            tok_type = TokenType.OPERATOR
        elif symbol == "(":
            tok_type = TokenType.OPEN_PARANTHESIS
        elif symbol == ")":
            tok_type = TokenType.CLOSE_PARANTHESIS
        elif Utilities.is_float(symbol):
            tok_type = TokenType.NUMBER

        return tok_type
