from core.Tokenizer import Tokenizer
from core.TokensUtil import Token, TokenType
from core.OperatorsUtil import OperatorFactory, Operator


class ExpressionValidator:
    """
    Class helps validate an In fix arithmetic expression. It tests for 3 things
    1. If the parentheses are balanced
    2. If there are any tokens from the expression that are of type Other (it only has operands, operators and '(' ')' )
    3. If every binary operator has left and right tokens that are Numbers (We only support BinaryOperators right now but
        the check for UnaryOperators will  check for left token being a number onlu. So it's extensible)
    """

    def __init__(self):
        self.op_factory = OperatorFactory()

    @staticmethod
    def __is_invalid_infix_operator(op: Operator, peek_left: int, peek_right: int, tokens: [Token]) -> bool:
        is_invalid = False
        tokens_count = len(tokens)

        left_operand = tokens[peek_left] if peek_left >= 0 else None
        right_operand = tokens[peek_right] if peek_right < tokens_count else None

        if op.is_binary() and left_operand and right_operand:
            is_invalid = left_operand.tok_type != TokenType.NUMBER or right_operand.tok_type != TokenType.NUMBER
        elif not op.is_binary() and right_operand:
            is_invalid = right_operand.tok_type != TokenType.NUMBER
        elif op.is_binary() and (left_operand is None or right_operand is None):
            is_invalid = True
        elif not op.is_binary() and right_operand is None:
            is_invalid = True

        return is_invalid

    @staticmethod
    def __has_balanced_paranthesis(expr: str) -> bool:
        stack = []
        for char in expr:
            if char == "(":
                stack.append(char)
            elif char == ")":
                if not stack:
                    return False
                else:
                    stack.pop()

        is_balanced = len(stack) == 0
        return is_balanced

    @staticmethod
    def __has_no_invalid_symbols(expr: str) -> bool:
        tokenizer = Tokenizer()
        tokens = tokenizer.get_tokens(expr)
        has_no_invalid_symbols = True

        for token in tokens:
            if token.tok_type == TokenType.OTHER:
                has_no_invalid_symbols = False
                break

        return has_no_invalid_symbols

    def __has_appropriate_operands_for_each_operator(self, expr: str) -> bool:
        expr_without_parenthesis = expr.replace("(", "").replace(")", "")
        tokenizer = Tokenizer()
        tokens = tokenizer.get_tokens(expr_without_parenthesis)
        has_valid_operands_for_operators = True

        cursor = 0
        tokens_count = len(tokens)
        while cursor < tokens_count:
            token = tokens[cursor]
            peek_left = cursor - 1
            peek_right = cursor + 1

            if token.tok_type == TokenType.OPERATOR:
                operator = self.op_factory.get_operator(token.tok_value)
                if ExpressionValidator.__is_invalid_infix_operator(operator, peek_left, peek_right, tokens):
                    has_valid_operands_for_operators = False
                    break

            cursor += 1

        return has_valid_operands_for_operators

    def is_valid_infix_expression(self, expr: str) -> bool:
        has_balanced_brackets = ExpressionValidator.__has_balanced_paranthesis(expr)
        has_no_invalid_symbols = ExpressionValidator.__has_no_invalid_symbols(expr)
        has_correct_operands = self.__has_appropriate_operands_for_each_operator(expr)

        is_valid = (has_balanced_brackets and has_no_invalid_symbols and has_correct_operands)

        return is_valid
