import enum

# Enum Defines TokenTypes for our Calculator
class TokenType(enum.Enum):
    OTHER = "other"
    NUMBER = "number"
    OPERATOR = "operator"
    OPEN_PARANTHESIS = "open_parenthesis"
    CLOSE_PARANTHESIS = "close_parenthesis"

# One Generic Token Class that holds value and Type is Enough for our usage, Keeps things Simpler than having Separate
# Concrete Classes of Each Token Type
class Token:
    def __init__(self, token_type: TokenType, value):
        self.__tok_value = value
        self.__tok_type = token_type

    @property
    def tok_value(self):
        return self.__tok_value

    @property
    def tok_type(self) -> TokenType:
        return self.__tok_type


