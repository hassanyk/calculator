import core.Constants as Constants
from abc import ABC, abstractmethod

# Abstract Operator class to force Concrete Operator Classes to implement perform_operation method. These classes can be
# extended to hold the Operator Associativity order for each Operator. Right now associativity wasn't in our scope but
# with this structure, it's fairly simple to extend.
class Operator(ABC):
    @abstractmethod
    def __init__(self, symbol):
        self.__symbol = symbol

    @property
    @abstractmethod
    def symbol(self):
        pass

    @symbol.setter
    @abstractmethod
    def symbol(self, value):
        pass

    @abstractmethod
    def is_binary(self):
        pass

    @abstractmethod
    def perform_operation(self, left_operand, right_operand):
        pass


# Abstract Child Classes that help us define Unary and Binary Operators
# Unary Operators take One Operand, Binary operand Take 2
class UnaryOperator(Operator):

    @abstractmethod
    def perform_operation(self, left_operand, right_operand=None):
        pass

    def is_binary(self):
        return False


class BinaryOperator(Operator):

    @abstractmethod
    def perform_operation(self, left_operand, right_operand):
        pass

    def is_binary(self):
        return True


# Concrete classes with Definitions for each Operator.
# Allows us extensibility to define future Operators like ^. Sin, Cos, Tan, % etc Without the need to change
# Implementation of the Evaluator too much
class AddOperator(BinaryOperator):
    def __init__(self, symbol):
        self.__symbol = symbol

    @property
    def symbol(self):
        return self.__symbol

    @symbol.setter
    def symbol(self, value):
        self.__symbol = value

    def perform_operation(self, left_operand, right_operand):
        return left_operand + right_operand


class MinusOperator(BinaryOperator):
    def __init__(self, symbol):
        self.__symbol = symbol

    @property
    def symbol(self):
        return self.__symbol

    @symbol.setter
    def symbol(self, value):
        self.__symbol = value

    def perform_operation(self, left_operand, right_operand):
        return left_operand - right_operand


class MultiplyOperator(BinaryOperator):
    def __init__(self, symbol):
        self.__symbol = symbol

    @property
    def symbol(self):
        return self.__symbol

    @symbol.setter
    def symbol(self, value):
        self.__symbol = value

    def perform_operation(self, left_operand, right_operand):
        return left_operand * right_operand


class DivideOperator(BinaryOperator):
    def __init__(self, symbol):
        self.__symbol = symbol

    @property
    def symbol(self):
        return self.__symbol

    @symbol.setter
    def symbol(self, value):
        self.__symbol = value

    def perform_operation(self, left_operand, right_operand):
        result = left_operand / right_operand if right_operand > 0 else 0
        return result

# Operator Factory to get instances of the Operator type we need
class OperatorFactory:
    def __init__(self):
        self.operators = {"+": AddOperator,
                          "-": MinusOperator,
                          "/": DivideOperator,
                          "*": MultiplyOperator}

    def get_operator(self, symbol) -> Operator:
        if symbol in Constants.OPERATORS_lIST:
            return self.operators[symbol](symbol)
        else:
            raise Exception(f"Unidentified operator '{symbol}' in expression string")