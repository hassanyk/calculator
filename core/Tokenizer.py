from core.Utilities import Utilities
from core.TokensUtil import Token, TokenType


class Tokenizer:
    """
    Class helps turn string expression to a List of Tokens with Token Value and Token Types
    """

    def get_tokens(self, expr: str) -> [Token]:
        tokens = []
        reader = iter(expr)

        # Extracting NUMBER token. Helps Support decimal Numbers
        while True:
            char = next(reader, "end")

            if str(char).isdigit() or char == ".":
                number = ""
                while char.isdigit() or char == ".":
                    number += char
                    char = next(reader, "end")
                if not Utilities.is_float(number):
                    raise Exception(f"Invalid number detected {number}")

                token = Token(token_type=TokenType.NUMBER, value=number)
                tokens.append(token)

            if char == "end":
                break
            # Extracting All Other token Types
            else:
                token_type = Utilities.get_token_type_from_symbol(char)
                token = Token(token_type=token_type, value=char)
                tokens.append(token)

        return tokens
