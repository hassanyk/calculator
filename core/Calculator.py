from core.Convertor import Convertor
from core.Evaluator import Evaluator
from core.Tokenizer import Tokenizer
from core.ExpressionValidator import ExpressionValidator


class Calculator:
    """
    Calculator processes simple arithmetic expressions with operators [+, -, *, /]
    A calculator is composed of:
    1. Validator: Helps Validate if the expression being passed to the calculator is Valid
    2. Tokenizer: to parse expression string into Tokens
    3. Convertor: Converts infix Tokens to PostFix Tokens
    4. Evaluator: Processes the postfic Tokens to calculate result
    """
    def __init__(self):
        self.tokenizer = Tokenizer()
        self.convertor = Convertor()
        self.evaluator = Evaluator()
        self.validator = ExpressionValidator()

    def __clean_expression(self, expr: str) -> str:
        stripped_expr = expr.replace(" ", "")
        return stripped_expr

    def calculate(self, expr: str) -> float:
        cleaned_expr = self.__clean_expression(expr)
        expr_result = 0

        if self.validator.is_valid_infix_expression(cleaned_expr):
            infix_tokens = self.tokenizer.get_tokens(cleaned_expr)
            postfix_tokens = self.convertor.get_postfix_tokens(infix_tokens)

            expr_result = self.evaluator.evaluate(postfix_tokens)

        return expr_result

    def is_valid_expression(self, expr) -> bool:
        return self.validator.is_valid_infix_expression(expr)
